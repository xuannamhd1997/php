<?php 
$person = array (
    array (
        "name"=>"An",
        "gender" => "Nam",
        "height"=> 170,
    ),
    array (
        "name"=>"Thành",
        "gender" => "Nam",
        "height"=> 180,
    ),
    array (
        "name"=>"Nam",
        "gender" => "Nam",
        "height"=> 180,
    ),
    array (
        "name"=>"Huyền",
        "gender" => "Nữ",
        "height"=> 156,
    ),
    array (
        "name"=>"Thư",
        "gender" => "Nữ",
        "height"=> 165,
    ),
    array (
        "name"=>"Đạt",
        "gender" => "Nam",
        "height"=> 170,
    ),
    array (
        "name"=>"Nga",
        "gender" => "Nữ",
        "height"=> 170,
    ),
    array (
        "name"=>"Thu",
        "gender" => "Nữ",
        "height"=> 160,
    ),
   array (
        "name"=>"Thuận",
        "gender" => "Nam",
        "height"=> 175,
    ),
);
?>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}
td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
<table>
  <tr>
    <th>STT</th>
    <th>Tên</th>
    <th>Giới tính</th>
    <th>Chiều cao</th>
  </tr>
<?php foreach ($person as $name => $info) { ?>
    <tr>
    <td> <?php echo $name+=1 ?></td>
    <td> <?php echo $info['name'] ?> </td>
    <td><?php echo $info["gender"] ?></td>
    <td><?php echo $info["height"] . " cm"?></td>
  </tr>
    <?php echo $info['name'] . " - " . $info["height"] ." cm";
        echo "<br>";
    ?>
<?php } ?>
  <?php $total_height = $total_height_male = $total_height_female = 0;
  foreach ($person as $name => $info) {
    $total_height+= $info['height'];
    if ($info['gender'] == "Nữ") {
        $total_height_female += $info['height'] ;
    }else {
        $total_height_male += $info['height'] ;
    }
  }
  echo 'TỔNG CHIỀU CAO TẤT CẢ : '.$total_height . " cm" . "<br>";
  echo 'TỔNG CHIỀU CAO NỮ : '.$total_height_female . " cm" . "<br>";
  echo 'TỔNG CHIỀU CAO NAM : '.$total_height_male . " cm" . "<br>";
 ?>
 <?php
    $arr1= array();
    foreach ($person as $name => $info) {
        $arr1[]=$info['height'];
    }
    foreach (array_count_values($arr1) as $height => $count) {
        if($count > 1) {
            echo  'Có ' . $count .' học sinh cùng chiều cao là '. $height .' cm , <br>';
        }
    } 
?>
</table>
</body>
</html>
